function addNumbers(num1, num2) {

	console.log("Displayed sum of " + num1 + " and " + num2);
	let sum = num1 + num2;
	console.log(sum);
}

addNumbers(5, 15);

function subtractNumbers(num1, num2) {
	console.log("Displayed difference of " + num1 + " and " + num2);
	let difference = num1 - num2;
	console.log(difference);
}

subtractNumbers(20, 5);

function multiplyNumbers(num1, num2) {

	console.log("The product of " + num1 + " and " + num2 +":");

	let product = num1 * num2;

	console.log(product);
	
	return product;
	
}

multiplyNumbers(50, 10);


function divideNumbers(num1, num2) {

	console.log("The quotient of " + num1 + " and " + num2 +":");

	let quotient = num1 / num2;

	console.log(quotient);
	
	return quotient;
	
}

divideNumbers(50, 10);

function getAreaCircle(radius){

	console.log("The result of getting the area of a circle with " + radius + " radius:");

	let circleArea = (3.14*(radius**2));
	console.log(circleArea);

	return circleArea;
}

getAreaCircle(15);

function getAverage(num1, num2, num3, num4) {

	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4);

	let averageVar = (num1 + num2 + num3 + num4)/4 ;
	console.log(averageVar);

	return averageVar;
}

getAverage(20,40,60,80);

function getPassingScore(num1, num2) {
	console.log("Is " + num1 + "/" + num2 + " a passing score?");

	let percentage = (num1/num2) * 100;
	let isPassed = percentage >= 75;
	let isPassingScore = isPassed;

	console.log(isPassingScore);

	return isPassingScore; 
}

getPassingScore(38, 50);