/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function

*/

/*
	Syntax:

	function functionName(parameter) {
		code block
	}
	functionName(argument);

*/

// name is called parameter
// "parameter" acts a name variable/container taht exists only inside of a function
// it is used to store information that is provided to a function when it is called/invoked.

function printName(name){
	console.log("My name is " + name);
}

// "Juana" and "Pogi", information/data provided directly into the function, it is called an argument.
printName("Juana");
printName("Pogi");

// variable as argument
let userName = "Elaine";

printName(userName);

// calling function with argument but without parameter

function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");

// Using Multiple Parameters
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " "+ lastName);
}

// In JS, providing more/less arguments than the expected parameters will not return an error
createFullName("Ako", "I", "Pogi");
createFullName("Eric", "Andales", "sobra", "Sobra");


function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini Activity:
		1. Create a function which is able to receive data as an argument.
			-This function should be able to receive the name of your favorite superhero
			-Display the name of your favorite superhero in the console.
*/
function superHero(heroName) {
	console.log("My favorite superhero is " + heroName);
}

superHero("my Parents");

/*
	Return Statement

	-The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.


*/

function returnFullName(firstName, middleName, lastName) {
	
	return firstName + " " + middleName + " " + lastName;
	
	console.log("Can we print this message?"); // This line will not be read because when we use "return" it means it is already the end statement, all succeeding codes will not be read after it.
}

// Whatever value that is returned from the "returnFullName" function can be stored in a variable

let completeName = returnFullName("John", "Doe", "Smith");
console.log(completeName);


function returnAddress(city, country) {
	let fullAddress = city + ", " + country;

	return fullAddress;
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");

console.log(myAddress);

/*
	Mini-Activity
		1. Debug our code. So that the function will be able to return a value and save it in a variable.

*/


// Difference when not using return statement
function printPlayerInformation(userName, level, job) {
	

	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: " + job);

	return userName + level	+ job;
}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1); //undefined
